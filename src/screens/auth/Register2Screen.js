import React, { Component } from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import colors from '../../utilities/colors'
import { Navigation } from 'react-native-navigation';
import EncryptedStorage from 'react-native-encrypted-storage';


class Register2Screen extends Component {

  static options() {
    return {
      topBar: {
        visible: false,
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      province: '',
      province_id: '',
      city: '',
      city_id: '',
      district: '',
      district_id: '',
      village: '',
      village_id: '',
      posyandu: '',
      posyandu_id: '',
      errorMessage: ''
    };
  }

  _onLogin() {
    console.log('masuk');
  }

  async _onRegister() {
    const form1 = this.props.form1
    const params = {
      name : form1.motherName,
      place_of_birth : form1.placeOfBirth,
      date_of_birth : form1.dateOfBirth,
      address : form1.address,
      phone : form1.phoneNumber,
      password : form1.password,
      province_id : this.state.province_id,
      city_id : this.state.city_id,
      district_id : this.state.district_id,
      village_id : this.state.village_id,
      posyandu_id : this.state.posyandu_id
    }
    try {
      const url = 'https://eposyandu.temanusaha.com/api/auth/register/'
      const response = await fetch(`${url}`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(params)
      })
      const json = await response.json();
      console.log(json);
      this._storeToken(json.meta.api_token)
    } catch (error) {
      console.log(error);
    }
  }

  async _storeToken(accessToken) {
    try {
      await EncryptedStorage.setItem("accessToken", accessToken)
      Navigation.setRoot({
        root: {
            stack: { 
                children: [
                    {
                        component: {
                            name: 'MainScreen'
                        }
                    }
                ]
            }
        }
    });
    } catch (error) {
      
    }
  }

  async _goToListScreen(type = '') {
    const error = []

    {
      Navigation.push(this.props.componentId, {
        component: {
          name: 'ListScreen',
          passProps: {
            type: type,
            province_id: this.state.province_id,
            city_id: this.state.city_id,
            district_id: this.state.district_id,
            village_id: this.state.village_id,
            posyandu_id: this.state.posyandu_id,
            _setDatas: (item, type) => this._setDatas(item, type)
          }
        },
      });
    }

  };

  _setDatas(item, type) {
    if (type === 'PROVINSI') {
      this.setState({
        province: item.name,
        province_id: item.id,
        city: '',
        city_id: '',
        district: '',
        district_id: '',
        village: '',
        village_id: '',
        posyandu: '',
        posyandu_id: '',
      })
    }
    if (type === 'KABUPATEN/KOTA') {
      this.setState({
        city: item.name,
        city_id: item.id,
        district: '',
        district_id: '',
        village: '',
        village_id: '',
        posyandu: '',
        posyandu_id: '',
      })
    }
    if (type === 'KECAMATAN') {
      this.setState({
        district: item.name,
        district_id: item.id,
        village: '',
        village_id: '',
        posyandu: '',
        posyandu_id: '',
      })
    }
    if (type === 'DESA') {
      this.setState({
        village: item.name,
        village_id: item.id,
        posyandu: '',
        posyandu_id: '',
      })
    }
    if (type === 'POSYANDU') {
      this.setState({
        posyandu: item.name,
        posyandu_id: item.id
      })
    }
  }

  render() {
    return (
      <ScrollView keyboardShouldPersistTaps={'handled'}>

        <Text style={styles.header}>
          DAFTAR
        </Text>

        <Text style={styles.subHeader}>
          Silahkan isi semua form dibawah ini
        </Text>

        <Text style={{ marginHorizontal: 16, color: colors.redColor }}>
          {this.state.errorMessage}
        </Text>

        <TextInput
          style={styles.input}
          value={this.state.province}
          placeholder="Provinsi"
          onFocus={() => this._goToListScreen('PROVINSI')}
        />

        <TextInput
          style={styles.input}
          value={this.state.city}
          placeholder="Kabupaten/Kota"
          onFocus={() => this._goToListScreen('KABUPATEN/KOTA')}
        />

        <TextInput
          style={styles.input}
          value={this.state.district}
          placeholder="Kecamatan"
          onFocus={() => this._goToListScreen('KECAMATAN')}
        />

        <TextInput
          style={styles.input}
          value={this.state.village}
          placeholder="Desa"
          onFocus={() => this._goToListScreen('DESA')}
        />

        <TextInput
          style={styles.input}
          value={this.state.posyandu}
          placeholder="Posyandu"
          onFocus={() => this._goToListScreen('POSYANDU')}
        />

        <TouchableOpacity
          onPress={() => this._onRegister()}>
          <Text style={styles.registerButton}>Selanjutnya</Text>
        </TouchableOpacity>

      </ScrollView>
    );
  }
};

const styles = StyleSheet.create({
  header: {
    margin: 16,
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.blackColor
  },
  subHeader: {
    margin: 16,
    marginTop: 0,
    fontSize: 16,
    color: colors.blackColor
  },
  input: {
    height: 40,
    marginHorizontal: 16,
    marginVertical: 8,
    borderBottomWidth: 1,
    padding: 12,
  },
  button: {
    alignItems: 'center',
    backgroundColor: colors.primaryColor,
    padding: 12,
    margin: 16,
    marginVertical: 20,
    borderRadius: 8
  },
  textButton: {
    color: colors.whiteColor,
    fontWeight: 'bold'
  },
  dontHaveAccount: {
    textAlign: 'center'
  },
  registerButton: {
    color: colors.primaryColor,
    textAlign: 'right',
    margin: 16
  }
});

export default Register2Screen;
