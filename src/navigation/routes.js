import { Navigation } from 'react-native-navigation';
import LoginScreen from '../screens/auth/LoginScreen';
import RegisterScreen from './../screens/auth/RegisterScreen';
import Register2Screen from './../screens/auth/Register2Screen';
import ListScreen from './../screens/auth/ListScreen';
import MainScreen from './../screens/main/MainScreen';
import App from './../../App';


export function routes(){
    Navigation.registerComponent('App', ()=>App)
    Navigation.registerComponent('LoginScreen', ()=>LoginScreen)
    Navigation.registerComponent('RegisterScreen', ()=>RegisterScreen)
    Navigation.registerComponent('Register2Screen', ()=>Register2Screen)
    Navigation.registerComponent('ListScreen', ()=>ListScreen)
    Navigation.registerComponent('MainScreen', ()=>MainScreen)
}