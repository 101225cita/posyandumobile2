import React, { Component } from 'react';
import {
  DatePickerAndroid,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import colors from '../../utilities/colors'
import { Navigation } from 'react-native-navigation';
import helpers from '../../utilities/helpers';

class RegisterScreen extends Component {

  static options() {
    return {
      topBar: {
        visible: false,
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      motherName: '',
      placeOfBirth: '',
      dateOfBirth: '',
      address: '',
      phoneNumber: '',
      password: ''
    };
  }

  _onLogin() {
    console.log('masuk');
  }

  _onNextRegister() {
    const form1 = this.state
    Navigation.push(this.props.componentId, {
      component: {
        name: 'Register2Screen',
        passProps: {
          form1
        }
      },
    });
  }

  async _openDateTimePicker() {
    try {
      const {
        action,
        year,
        month,
        day
      } = await DatePickerAndroid.open({
        mode: 'spinner',
        date: new Date()
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        this.setState({ dateOfBirth: helpers.translateDate(year, month, day) })
      }
    } catch ({ code, message}) {
      console.worn('Cannot open date picker', message);
    }
  }

  render() {
    return (
      <ScrollView keyboardShouldPersistTaps={'handled'}>

        <Text style={styles.header}>
          DAFTAR
        </Text>

        <Text style={styles.subHeader}>
          Silahkan isi semua form dibawah ini
        </Text>

        <TextInput
          style={styles.input}
          value={this.state.motherName}
          onChangeText={(motherName) => this.setState({ motherName })}
          placeholder="Nama Lengkap Ibu"
          keyboardType="default"
          autoCapitalize='words'
        />
        <View style={{ flexDirection: 'row', flex: 1 }}>
          <TextInput
            style={[styles.input, { flex: 1 }]}
            value={this.state.placeOfBirth}
            onChangeText={(placeOfBirth) => this.setState({ placeOfBirth })}
            placeholder="Tempat Lahir"
            keyboardType="default"
            autoCapitalize='words'
          />

          <TextInput
            style={[styles.input, { flex: 1 }]}
            value={this.state.dateOfBirth}
            onChangeText={(dateOfBirth) => this.setState({ dateOfBirth })}
            placeholder="Tanggal Lahir"
            keyboardType="default"
            autoCapitalize='words'
            onFocus={() => this._openDateTimePicker()}
          />
        </View>
        

        <TextInput
          style={styles.input}
          value={this.state.address}
          onChangeText={(address) => this.setState({ address })}
          placeholder="Alamat"
          keyboardType="default"
          autoCapitalize='words'
        />

        <TextInput
          style={styles.input}
          value={this.state.phoneNumber}
          onChangeText={(phoneNumber) => this.setState({ phoneNumber })}
          placeholder="Nomor Telepon"
          keyboardType="numeric"
        />

        <TextInput
          style={styles.input}
          value={this.state.password}
          onChangeText={(password) => this.setState({ password })}
          placeholder="Password"
          keyboardType="default"
          autoCapitalize='none'
          secureTextEntry
        />

        <TouchableOpacity
          onPress={() => this._onNextRegister()}>
          <Text style={styles.registerButton}>Selanjutnya</Text>
        </TouchableOpacity>

      </ScrollView>
    );
  }
};

const styles = StyleSheet.create({
  header: {
    margin: 16,
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.blackColor
  },
  subHeader: {
    margin: 16,
    marginTop: 0,
    fontSize: 16,
    color: colors.blackColor
  },
  input: {
    height: 40,
    marginHorizontal: 16,
    marginVertical: 8,
    borderBottomWidth: 1,
    padding: 12,
  },
  button: {
    alignItems: 'center',
    backgroundColor: colors.primaryColor,
    padding: 12,
    margin: 16,
    marginVertical: 20,
    borderRadius: 8
  },
  textButton: {
    color: colors.whiteColor,
    fontWeight: 'bold'
  },
  dontHaveAccount: {
    textAlign: 'center'
  },
  registerButton: {
    color: colors.primaryColor,
    textAlign: 'right',
    margin: 16
  }
});

export default RegisterScreen;
