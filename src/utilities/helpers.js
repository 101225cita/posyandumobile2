import { Component } from "react";

export default class helpers extends Component {

    static translateDate(year, month, day) {
        month = month + 1
        month = month < 10 ? '0' + month : month
        day = day < 10 ? '0' + day : day
        return `${year}-${month}-${day}`
    }
}