import React, { Component } from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import colors from '../../utilities/colors'
import { Navigation } from 'react-native-navigation';
import EncryptedStorage from 'react-native-encrypted-storage';

class MainScreen extends Component {

  static options() {
    return {
      topBar: {
        visible: false,
      },
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      x: 1,
      y: 2,
      phoneNumber: '',
      phoneNumberResult: '',
      password: ''
    };
  }

  _onLogout() {
    EncryptedStorage.removeItem('accessToken')
    Navigation.setRoot({
      root: {
          stack: { 
              children: [
                  {
                      component: {
                          name: 'LoginScreen'
                      }
                  }
              ]
          }
      }
  });
  }

  render() {
    return (
      <ScrollView keyboardShouldPersistTaps={'handled'}>
          <Text style={styles.header}>
              MAIN SCREEN
          </Text>
        
      <TouchableOpacity
        onPress={() => this._onLogout()}>
          <Text style={styles.registerButton}>Keluar</Text>
      </TouchableOpacity>
      </ScrollView>
    );
  }
};

const styles = StyleSheet.create({
  header: {
    margin: 16,
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.blackColor
  },
  input: {
    height: 40,
    marginHorizontal: 16,
    marginVertical: 8,
    borderBottomWidth: 1,
    padding: 12,
  },
  button: {
    alignItems: 'center',
    backgroundColor: colors.primaryColor,
    padding: 12,
    margin: 16,
    marginVertical: 20,
    borderRadius: 8
  },
  textButton: {
    color: colors.whiteColor,
    fontWeight: 'bold'
  },
  dontHaveAccount: {
    textAlign: 'center'
  },
  registerButton: {
    color: colors.primaryColor,
    textAlign: 'center',
    marginVertical: 16
  }
});

export default MainScreen;
